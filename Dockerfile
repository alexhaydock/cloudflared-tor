FROM alpine:3
LABEL maintainer "Alex Haydock <alex@alexhaydock.co.uk>"
LABEL name "cloudflared-tor"
LABEL version "1.0"

RUN apk --no-cache add tor shadow socat \
    && usermod -u 7942 -o tor \
    && apk del shadow

# Add Tor config
COPY --chown=tor:nogroup torrc /etc/tor/torrc

# Add entrypoint script
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Runtime settings
ENTRYPOINT ["/entrypoint.sh"]